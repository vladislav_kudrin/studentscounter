package me.studentscounter;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;
import android.view.View;
import android.os.Bundle;
import java.util.Locale;

/**
 * Runs an application's activity.
 *
 * @author Vladislav
 * @version 1.1
 * @since 02/17/2020
 */
public class MainActivity extends AppCompatActivity {
    private int counter = 0;
    private TextView counterView;

    /**
     * Creates the application's activity.
     *
     * @param savedInstanceState previous saved data states.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        counterView = findViewById(R.id.textCounter);
    }

    /**
     * Restores data states from {@code savedInstanceState}.
     *
     * @param savedInstanceState data states for restoring.
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        counter = (savedInstanceState.getInt("COUNTER_STATE_KEY"));
        counterView.setText(String.format(Locale.getDefault(), "%d", counter));
    }

    /**
     * Saves data states to {@code outState}.
     *
     * @param outState data states for saving.
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("COUNTER_STATE_KEY", counter);
        super.onSaveInstanceState(outState);
    }

    /**
     * Increases value of {@code counter} by 1 and sets new value as a text to {@code counterView}.
     *
     * @param view user interface components.
     */
    public void onClickButtonAddStudent(View view) {
        counter++;
        counterView.setText(String.format(Locale.getDefault(), "%d", counter));
    }
}